from django.urls import path
from .views import CartView
from .views import CheckoutView
from .views import ThankYouView

app_name = 'orders'

urlpatterns = [
    path('cart/', CartView.as_view(), name='cart'),
    path('checkout/', CheckoutView.as_view(), name='checkout'),
    path('thank_you/', ThankYouView.as_view(), name='thank_you'),
]
