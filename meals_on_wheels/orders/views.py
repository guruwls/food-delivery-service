from django.views.generic.list import ListView
from django.views.generic import TemplateView
from products.models import Product
from .models import ItemInCart


class CartView(ListView):
    model = ItemInCart
    context_object_name = 'item'
    template_name = 'orders/cart.html'


class CheckoutView(TemplateView):
    template_name = 'orders/checkout.html'


class ThankYouView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'orders/thank_you.html'

    def get_queryset(self):
        return super().get_queryset().order_by('-views')[:5]
