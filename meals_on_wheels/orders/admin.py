from django.contrib import admin
from .models import Order, ItemInCart

admin.site.register(Order)
admin.site.register(ItemInCart)
