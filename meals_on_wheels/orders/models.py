from django.db import models
from django.db.models.deletion import DO_NOTHING, SET_NULL
from products.models import Product
from customers.models import Customer


class Order(models.Model):
    customer = models.ForeignKey(
                                 Customer,
                                 on_delete=DO_NOTHING,
                                 related_name='history',
                                 blank=True,
                                 null=True,
                                 )
    total = models.DecimalField(
                                max_digits=12,
                                decimal_places=2,
                                blank=True,
                                null=True,
                                )
    payment_option = models.CharField(
                                      max_length=100,
                                      blank=True,
                                      null=True,
                                      )
    opened_on = models.DateTimeField(auto_now_add=True)
    closed_on = models.DateTimeField(auto_now=True)
    completed = models.BooleanField(default=False)


class ItemInCart(models.Model):
    product = models.ForeignKey(
                                Product,
                                on_delete=SET_NULL,
                                blank=True,
                                null=True,
                                )
    customer = models.ForeignKey(
                                 Customer,
                                 on_delete=SET_NULL,
                                 blank=True,
                                 null=True,
                                 related_name='cart'
                                 )
    order = models.ForeignKey(
                              Order,
                              on_delete=SET_NULL,
                              blank=True,
                              null=True,
                              related_name='items'
                              )
    quantity = models.PositiveSmallIntegerField(blank=True)

    def __str__(self):
        return self.product

    class Meta:
        verbose_name = 'items in cart'
