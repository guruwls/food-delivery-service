from django.contrib import admin
from .models import Customer, BankCard

admin.site.register(Customer)
admin.site.register(BankCard)
