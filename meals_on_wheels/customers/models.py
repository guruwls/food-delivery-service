from django.db import models
# from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db.models.deletion import CASCADE
from django.db.models.fields import CharField


class Customer(AbstractUser):
    phone = models.CharField(max_length=20, blank=True)
    city = models.CharField(max_length=225, blank=True)
    street_address = models.CharField(max_length=225, blank=True)
    bonus_account = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.username


class BankCard(models.Model):
    number = models.CharField(max_length=16)
    expiration_date = models.DateField()
    secret_code = CharField(max_length=3)
    customer = models.ForeignKey(
                                 Customer,
                                 on_delete=CASCADE,
                                 blank=True,
                                 null=True,
                                 related_name='cards'
                                 )

    def __str__(self):
        return self.number
