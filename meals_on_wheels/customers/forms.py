from django.contrib.auth import get_user_model
from django.forms import ModelForm
from django.forms import widgets


class ProfileForm(ModelForm):
    class Meta:
        model = get_user_model()
        fields = (
                  'username',
                  'phone',
                  'first_name',
                  'last_name',
                  'email',
                  'city',
                  'street_address'
                  )

        widgets = {
                   'email':
                   widgets.EmailInput(attrs={
                                            'placeholder':
                                            'Type in your email address',
                                            }),
                   'first_name':
                   widgets.TextInput(attrs={
                                           'placeholder':
                                           'Type in your first name',
                                           }),
                   'last_name':
                   widgets.TextInput(attrs={
                                           'placeholder':
                                           'Type in your last name',
                                           }),
                   'phone':
                   widgets.TextInput(attrs={
                                           'placeholder':
                                           'Type in your phone number',
                                           }),
                   'city':
                   widgets.TextInput(attrs={
                                           'placeholder':
                                           'Type in your city',
                                           }),
                   'street_address':
                   widgets.TextInput(attrs={
                                           'placeholder':
                                           'Type in your street address',
                                           }),
        }

        labels = {
                'email': 'Email',
        }
