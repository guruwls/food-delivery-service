from django.contrib.auth.forms import UserCreationForm, UsernameField
from django.contrib.auth.views import LoginView
from django.contrib.auth import get_user_model
from django.contrib.auth import login
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import SetPasswordForm
from django.contrib import messages
from django.views.generic import DetailView
from django.views.generic.edit import FormView
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from .forms import ProfileForm


class LoginCustomerView(LoginView):
    template_name = 'customers/login_register.html'
    page = 'login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page'] = self.page
        return context


class RegisterView(FormView):
    template_name = 'customers/login_register.html'
    success_url = '/'

    class CustomUserCreationForm(UserCreationForm):
        class Meta:
            model = get_user_model()
            fields = ("username",)
            field_classes = {'username': UsernameField}

    form_class = CustomUserCreationForm

    def form_valid(self, form):
        form.save()
        login(self.request, get_user_model()
              .objects.get(username=self.request.POST['username']))
        return super().form_valid(form)


class ProfileDetailsView(LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('customers:login')
    template_name = 'customers/profile.html'
    model = get_user_model()
    context_object_name = 'customer'
    page = 'profile_details'

    def get_object(self):
        return get_user_model().objects.get(pk=self.request.user.pk)

    def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data(**kwargs)
        context['page'] = self.page
        return context


class ProfileEditView(LoginRequiredMixin, FormView):
    login_url = reverse_lazy('customers:login')
    template_name = 'customers/profile.html'
    form_class = ProfileForm
    success_url = reverse_lazy('customers:profile-details')

    def form_invalid(self, form):
        messages.error(self.request, 'Something went wrong...')
        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_form_kwargs(self) -> dict:
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = get_object_or_404(
                                               get_user_model(),
                                               pk=self.request.user.pk
                                               )
        return kwargs


class CustomPasswordChangeView(LoginRequiredMixin, FormView):
    login_url = reverse_lazy('customers:login')
    template_name = 'customers/password_change.html'
    form_class = SetPasswordForm

    def get_form_kwargs(self) -> dict:
        kwargs = super().get_form_kwargs()
        kwargs['user'] = get_object_or_404(get_user_model(),
                                           pk=self.request.user.id
                                           )
        return kwargs

    def form_valid(self, form):
        user = form.save()
        update_session_auth_hash(self.request, user)
        messages.success(self.request,
                         'Your password has been successfully updated!'
                         )
        return redirect(reverse_lazy('customers:profile-details'))
