from django.db import models
from django.db.models.deletion import CASCADE


class Product(models.Model):
    name = models.CharField(max_length=100)
    overview = models.TextField(
                                max_length=200,
                                blank=True,
                                null=True,
                                )
    description = models.TextField(
                                   max_length=500,
                                   blank=True,
                                   null=True,
                                   )
    price = models.DecimalField(
                                max_digits=12,
                                decimal_places=2,
                                blank=True,
                                )
    category = models.ManyToManyField(
                                      'Category',
                                      related_name='products',
                                      )
    ingredient = models.ManyToManyField(
                                        'Ingredient',
                                        related_name='products'
                                        )
    cover_image = models.ImageField(blank=True, null=True)
    views = models.IntegerField(default=0, blank=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    # quantity_in_stock = models.IntegerField()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-date_added', '-views', 'name']

    @property
    def imageURL(self):
        try:
            url = self.cover_image.url
        except:
            url = ''
        return url


class Category(models.Model):
    name = models.CharField(max_length=100)
    ordinal = models.PositiveSmallIntegerField(blank=True, null=True)

    class Meta:
        verbose_name_plural = 'categories'
        ordering = ['ordinal']

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Photo(models.Model):
    image = models.ImageField(blank=True, null=True)
    product = models.ForeignKey(
                                Product,
                                on_delete=CASCADE,
                                related_name='photos'
                                )
