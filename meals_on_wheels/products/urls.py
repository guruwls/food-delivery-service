from django.urls import path
from products.views import ProductListView, ProductDetailView

app_name = 'products'

urlpatterns = [
    path('menu/', ProductListView.as_view(), name='menu'),
    path('product/<int:product_id>',
         ProductDetailView.as_view(),
         name='product'
         ),
]
