from django import template
from products.models import Category

register = template.Library()


@register.inclusion_tag('products/chunks/sidebar.html')
def category_list():
    return {'categories': Category.objects.all()}
