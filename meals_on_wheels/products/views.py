from django.views.generic.list import ListView
from django.views.generic import DetailView
from products.models import Product


class ProductListView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'products/menu.html'

    def get_queryset(self):
        queryset = super().get_queryset()
        if 'category' in self.request.GET:
            queryset = queryset.filter(
                       category=self.request.GET['category']
                                       )
        return queryset


class ProductDetailView(DetailView):
    model = Product
    template_name = 'product.html'
    pk_url_kwarg = 'product_id'

    def get(self, request, product_id):
        response = super().get(request)
        self.object.views += 1
        self.object.save()
        return response
